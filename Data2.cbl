       IDENTIFICATION DIVISION.
       PROGRAM-ID.  DATA2.
       AUTHOR. SARAWUT.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 AL-NUM       PIC      X(5)      VALUE "A1234".
       01 NUM-INT      PIC      9(5).
       01 NUM-NON-INT  PIC      9(3)V9(2).
       01 ALPHA        PIC      A(5).
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "ALPHA-NUM : " AL-NUM 
           MOVE AL-NUM TO NUM-INT
           DISPLAY "ALPHA-NUM TO INT : " NUM-INT
           MOVE AL-NUM TO NUM-NON-INT
           DISPLAY "ALPHA-NUM TO NON INT : " NUM-NON-INT
           MOVE AL-NUM TO ALPHA 
           DISPLAY "ALPHA-NUM TO ALPHA : " ALPHA
           .